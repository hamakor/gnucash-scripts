from functools import reduce
import gnucash as gc


class Gnucash(object):

    def __init__(self, book_uri):
        self._session = gc.Session(book_uri=book_uri)
        self._book = self._session.book
        self._root_account = self._book.get_root_account()

        commodity_table = self._book.get_table()
        self.ILS = commodity_table.lookup("ISO4217", "ILS")
        self.USD = commodity_table.lookup("ISO4217", "USD")
        self.EUR = commodity_table.lookup("ISO4217", "EUR")

        self.__seendict = {}

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        if type is None:
            # No exception. We can save
            self._session.save()
        # Either way the session needs to end
        self._session.end()

    def save(self):
        """For explicit saving. It is recommended to trust the contextmanager"""
        self._session.save()

    def seen(self, acct, num):
        if repr(acct) not in self.__seendict:
            l = []
            for split in acct.GetSplitList():
                txn = split.parent
                if txn.GetNum() not in l:
                    l.append(txn.GetNum().strip())
            self.__seendict[repr(acct)] = l

        return num in self.__seendict[repr(acct)]

    def account(self, account_path):
        root = self._root_account
        account = reduce(
            lambda acc,nam: (acc.lookup_by_name(nam) if acc else None),
            account_path.split(":"),
            root
        )
        if not account:
            raise KeyError("Account '%s' not found" % account_path)
        return account

    def new_transaction(self, currency):
        currency_obj = getattr(self, currency)
        return Transaction(self._book, currency_obj)


class Transaction(object):

    def __init__(self, book, currency):
        self._t = gc.Transaction(book)
        self._book = book # saved for adding splits
        self._t.BeginEdit()
        # Note this works on self._t
        self.currency = currency

    def __getattr__(self, attr):
        """Only called for attributes not defined on the object"""
        getter = getattr(self._t, 'Get%s' % attr.capitalize())
        return getter()

    def __setattr__(self, attr, value):
        if attr.startswith('_'):
            # Do a normal set
            object.__setattr__(self, attr, value)
            return
        try:
            setter = getattr(self._t, 'Set%s' % attr.capitalize())
        except AttributeError:
            object.__setattr__(self, attr, value)
        else:
            #if isinstance(value, str):
            #    value = value.encode('utf-8')
            setter(value)

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        if not type:
            # No exception, but can we commit?
            if self.is_ready_to_commit():
                self._t.CommitEdit()
            else:
                self._t.RollbackEdit()
                raise ValueError("Transaction imbalanced")
        else:
            # We're rolling back everything
            self._t.RollbackEdit()

    def set_date(self, day, month, year):
        self._t.SetDate(day, month, year)

    def add_split(self, acct, amount):
        s = gc.Split(self._book)
        s.SetParent(self._t)
        s.SetAccount(acct)
        value = money_value(amount)
        s.SetAmount(value)
        s.SetValue(value)
        return s

    def is_ready_to_commit(self):
        for split in self._t.GetSplitList():
            if split.GetValue().to_double() == 0.0:
                split.Destroy()

        return self._t.IsBalanced()


def money_value(value, denom=100):
    s = int(round(value*denom))
    return gc.GncNumeric(s, denom)
