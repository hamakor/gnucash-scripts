Gnucash Scripts
===============

This is now essentially one script... Importing PayPal transactions
into Gnucash.

Requirements
------------

Python 2.7
  Python 2 required because of...

python-gnucash
  which is not pip-installable. On Debian-based systems,
  ``apt install python-gnucash``

unicodecsv
  which is pip-installable, but we can't use that anyway.
  On Debian-based systems, ``apt install python-unicodecsv``

Usage
-----

Clone this repository, then::

  $ python transactions-paypal.py -h
  usage: transactions-paypal.py [-h] [--for-real] GNUCASH PAYPAL

  import PayPal transactions from CSV to Gnucash

  positional arguments:
    GNUCASH     Path to GnuCash file
    PAYPAL      Path to PayPal CSV file

  optional arguments:
    -h, --help  show this help message and exit
    --for-real  Without this, nothing really gets saved

Limitations
-----------

Currently only supports one currency. Code exists to select it, but
this is not available through command line.

It is very verbose -- spews everything to stdout. When It succeeds, it's
"New Transaction: *details*". Otherwise, it complains. Known complaints
include foreign currency and unidentifiable target accounts.

When not given the ``--for-real`` flag, a stack trace goes on stderr.
This is ok, as you'll realize when looking at it.

Good points
-----------

Non-completed transactions are ignored. They didn't really happen.


