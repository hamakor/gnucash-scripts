#!/usr/bin/python3
#
# Requires python3-gnucash (obviously, but not pip-installable)

import codecs
import sys
from decimal import Decimal

#import unicodecsv as csv
import csv
from gnucashlib import Gnucash

def guess_accounts(row):
    """
    Given a row from the PayPal CSV file, decide which account the
    main leg of the transaction is counted against, and which account
    the fees get counted against.

    Adapt this as needed.
    """
    # Guess the to-account and fee-account
    # Defaults
    account = None
    fee_account = "Expenses:PayPal"
    
    title = row['Item Title']
    
    if 'PyCon Israel 2018 Ticket Purchase' in title:
        account = "Income:PyCon Israel:Tickets"
        fee_account = "Expenses:PyCon Israel:PayPal"
    elif 'August Penguin 2018 Ticket Purchase' in title:
        account = "Income:August Penguin:Tickets"
        fee_account = "Expenses:August Penguin:PayPal"
    elif 'FLIP 2018 Ticket Purchase' in title:
        account = "Income:Flip:Tickets"
        fee_account = "Expenses:Flip:PayPal"
    elif 'Core C++ 2019 Ticket Purchase' in title:
        account = "Income:Core C++:Tickets"
        fee_account = "Expenses:Core C++:PayPal"
    elif 'Hamakor Membership Fee' in title:
        account = "Income:Membership"
        fee_account = "Expenses:PayPal"  # Make the default explicit
    return account, fee_account


def get_ref_txn_accounts(row, orig_txns):
    ref_num = row['Reference Txn ID']
    orig_row = orig_txns[ref_num]
    return orig_row['account'], orig_row['fee_account']


def import_txns(gc, data,
                paypal_account="Assets:Current Assets:PayPal Account",
                primary_currency="ILS",
                on_other_currency="report"):

    paypal_acct = gc.account(paypal_account)

    orig_txns = {}
    for row in data:
        if row['Status'] != 'Completed':
            continue

        day, month, year = [int(f) for f in row['Date'].split('/')]
        date = '%04d-%02d-%02d' % (year, month, day)

        num = row['Transaction ID']

        net =   Decimal(row['Net'].replace(',', ''))
        gross = -Decimal(row['Gross'].replace(',', ''))
        fee =   -Decimal(row['Fee'].replace(',', ''))

        if row['Type']=='Payment Refund':
            description = "Refund: " + row['Name']
            account, fee_account = get_ref_txn_accounts(row, orig_txns)
        else:
            description = row['Item Title'] + ": " + row['Name']
            account, fee_account = guess_accounts(row)
            # Record the accounts -- note PayPal field names are all capitalized
            row['account'] = account
            row['fee_account'] = fee_account
            orig_txns[num] = row

        currency = row['Currency']

        if currency != primary_currency:
            if on_other_currency != 'report':
                raise NotImplementedError("Only 'report' supported for other currencies")
            print ("Other Currency Transaction:", num, currency, date, description, gross, account)
            continue

        if not account:
            print ("Failed to guess account:", num, currency, date, description, gross, account)
            continue
    
        to_acct = gc.account(account)
        fee_acct = gc.account(fee_account)
            
        if not gc.seen(paypal_acct, num):
            print ("New Transaction:", num, currency, date, description, gross, account)
            with gc.new_transaction(currency) as t:
                t.num = num
                t.set_date(day, month, year)
                t.description = description
                t.add_split(paypal_acct, net)
                t.add_split(to_acct, gross)
                t.add_split(fee_acct, fee)
                # Transaction saved here if all went well


def main(gcfile, csvfile, dry_run=True):
    # TODO: add passing of more args from command line

    # Read in CSV from PayPal
    infile = open(csvfile, 'r')
    infile.seek(3) # skip sig
    incsv = csv.DictReader(infile, skipinitialspace=True)
    with Gnucash(gcfile) as gc:
        import_txns(gc, incsv)
        if dry_run:
            raise Exception("ha ha dry run, cancelling")
        # If all went well, GnuCash file is saved here

        
if __name__=='__main__':
    import argparse
    parser = argparse.ArgumentParser(description="import PayPal transactions from CSV to Gnucash")
    parser.add_argument(dest='gcfile', metavar='GNUCASH',
                        help='Path to GnuCash file')
    parser.add_argument(dest='csvfile', metavar='PAYPAL',
                        help='Path to PayPal CSV file')
    parser.add_argument('--for-real', action='store_false', dest='dry_run',
                        help='Without this, nothing really gets saved')

    args = parser.parse_args()
    main(**vars(args))

